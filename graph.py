from rule import Rule


class Graph:
    def __init__(self, facts=[], rules=[]):
        self.facts = facts
        self.rules = rules

    def load_from_file(self, filename):
        with open(filename) as f:
            lines = f.readlines()
        lines = [l.strip() for l in lines]
        self.facts = lines.pop(0).split(' ')
        while lines:
            rule = lines.pop(0)
            parsed_rule = rule.split('=>')
            self.rules.append(Rule(parsed_rule[1].strip(), parsed_rule[0].strip().split(' ')))

    def depth_search_first(self, inputs, target):
        print()
        print('===================')
        print('Depth Search First:')
        print('===================')
        working_memory = set(inputs)
        open_rules_ids = [i for i in range(len(self.rules))]
        stack_rules_ids = []
        self.update_rules_list(working_memory, open_rules_ids, stack_rules_ids)

        while stack_rules_ids:
            self.print_current_state(stack_rules_ids[::-1], working_memory)
            current_rule_id = stack_rules_ids.pop()
            parent_node = self.rules[current_rule_id].parent
            working_memory.add(parent_node)
            if parent_node == target:
                return True
            self.update_rules_list(working_memory, open_rules_ids, stack_rules_ids)
        return False

    def breadth_first_search(self, inputs, target):
        print()
        print('=====================')
        print('Breadth Search First:')
        print('=====================')
        working_memory = set(inputs)
        open_rules_ids = [i for i in range(len(self.rules))]
        queue_rules_ids = []
        self.update_rules_list(working_memory, open_rules_ids, queue_rules_ids)

        while queue_rules_ids:
            self.print_current_state(queue_rules_ids, working_memory)
            current_rule_id = queue_rules_ids.pop(0)
            parent_node = self.rules[current_rule_id].parent
            working_memory.add(parent_node)
            if parent_node == target:
                return True
            self.update_rules_list(working_memory, open_rules_ids, queue_rules_ids)
        return False

    def update_rules_list(self, working_memory, open_rules_ids, rules_list_ids):
        i = 0
        while i < len(open_rules_ids):
            if self.is_children_in_working_memory(working_memory, self.rules[open_rules_ids[i]].children):
                rules_list_ids.append(open_rules_ids[i])
                open_rules_ids.pop(i)
                i -= 1
            i+= 1

    def is_children_in_working_memory(self, working_memory, children):
        cs = set(children)
        res1 = cs.issubset(working_memory)
        res2 = working_memory.issubset(cs)
        return set(children).issubset(working_memory)

    def print_current_state(self, rules_list_ids, working_memory):
        print('Rules in memory: ', end='')
        for rule_id in rules_list_ids:
            print(str(int(rule_id) + 100) + ' ', end='')
        print()

        print('Working memory: ', end='')
        for node in sorted(working_memory):
            print(node + ' ', end='')
        print()
        print()

