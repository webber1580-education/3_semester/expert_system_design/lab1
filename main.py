from graph import Graph


def main():
    graph = Graph()
    graph.load_from_file('test_data')
    inputs = input('Input facts: ').split(' ')
    target = input('Input target fact: ')

    is_reachable = graph.breadth_first_search(inputs, target)
    is_reachable = graph.depth_search_first(inputs, target)
    if is_reachable:
        print('Target fact is reachable')
    else:
        print('target fact is not reachable')


main()